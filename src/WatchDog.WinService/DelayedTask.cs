﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace WatchDog.WinService
{
    //public class BackgroundJob
    //{
    //    private CancellationTokenSource _cts =new CancellationTokenSource();
    //    public CancellationToken Token { get; private set; }

    //    private ConcurrentDictionary<string, Task> _cache = new ConcurrentDictionary<string, Task>();
    //    private ConcurrentDictionary<string, Task> _cache2 = new ConcurrentDictionary<string, Task>();

    //    private void AddDelayStopTask(int a, string name)
    //    {
    //        if (!_cache.Any(c => c.Key == name))
    //        {
    //            Token = _cts.Token;

    //            var task = Task.Run(async () =>
    //            {
    //                try
    //                {
    //                    await Task.Delay(a, Token);
    //                    if (!Token.IsCancellationRequested && ProcessHelper.IsRuning(name))
    //                    {
    //                        Log.Logger.Debug($"定时关闭任务：正在完成对程序{name}的关闭（定时{a}毫秒）");
    //                        ProcessHelper.Stop(name);
    //                    }
    //                    else
    //                    {
    //                        var s = Token.IsCancellationRequested ? "任务被取消" : "程序未运行";
    //                        Log.Logger.Debug($"定时关闭任务(取消)：取消对程序{name}的定时关闭任务（定时{a}毫秒）,取消原因：{s}");
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    Log.Logger.Error(ex, "执行定时关闭任务时出错");
    //                }
    //            }, Token);
    //            _cache.AddOrUpdate(name, task, (k, v) => v);
    //        }
    //        else
    //        {
    //            Log.Logger.Debug($"重复定时关闭任务(跳过):{name}");
    //        }
    //    }
    //    private void AddDelayStartTask(int a, string name)
    //    {
    //        if (!_cache2.Any(c => c.Key == name))
    //        {
    //            var task = Task.Run(async () =>
    //            {
    //                try
    //                {
    //                    await Task.Delay(a, Token);
    //                    if (!Token.IsCancellationRequested)
    //                    {
    //                        Log.Logger.Debug($"延迟启动任务：正在完成对程序{name}的启动（延迟{a}毫秒）");
    //                        ProcessHelper.Start(name);
    //                    }
    //                    else
    //                    {
    //                        Log.Logger.Debug($"延迟启动任务(取消)：取消对程序{name}的延迟启动任务（延迟{a}毫秒）");
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    Log.Logger.Error(ex, "执行延迟启动任务时出错");
    //                }
    //            }, Token);
    //            _cache2.AddOrUpdate(name, task, (k, v) => v);
    //        }
    //        else
    //        {
    //            Log.Logger.Debug($"重复延迟启动任务(跳过):{name}");
    //        }

    //    }

    //}


    //public class RecurringJob
    //{
    //    private CancellationTokenSource cts1 = new CancellationTokenSource();
    //    private CancellationToken token = new CancellationToken();
    //    public string Add(int millisecondsDelay, Func<int> func, Action action)
    //    {
    //        token = cts1.Token;

    //        Task.Run(async () =>
    //        {
    //            while (!token.IsCancellationRequested)
    //            {
    //                await Task.Delay(5 * 60 * 1000, token);
    //                Task.Run(() =>
    //                {
    //                    var xx = func();
    //                    action.Invoke();
    //                }).ConfigureAwait(false);
    //            }
    //        }, token);
    //        return Guid.NewGuid().ToString();
    //    }

    //    public void Remove()
    //    {
    //        cts1.Cancel();
    //    }

    //}

    //public class RecurringJobDto
    //{
    //    public string Id { get; set; }

    //    public RecurringJob Job { get; set; }
    //}

    //public static class RecurringJobManager
    //{
    //    private static ConcurrentDictionary<string, RecurringJobDto> _jobs = new ConcurrentDictionary<string, RecurringJobDto>();

    //    public static string AddOrUpdate(string recurringJobId, int millisecondsDelay, Action action)
    //    {
    //        if (_jobs.ContainsKey(recurringJobId))
    //        {
    //            var job = _jobs.First(c => c.Key == recurringJobId).Value.Job;
    //            job.Remove();
    //            job.Add(millisecondsDelay, action);
    //            return recurringJobId;
    //        }
    //        else
    //        {
    //            var job = new RecurringJob();
    //            var jobId = job.Add(millisecondsDelay, action);
    //            var s = new RecurringJobDto()
    //            {
    //                Id = jobId,
    //                Job = job,
    //            };
    //            return jobId;
    //        }
    //    }
    //    public static void RemoveIfExists(string recurringJobId)
    //    {
    //        if (_jobs.ContainsKey(recurringJobId))
    //        {
    //            var job = _jobs.First(c => c.Key == recurringJobId).Value.Job;
    //            job.Remove();
    //        }
    //    }
    //}


   

    //BackgroundJob.Schedule(
    //() => Console.WriteLine("Delayed!"),
    //TimeSpan.FromDays(7));
}
