﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WatchDog.WatchItemManages;

namespace WatchDogApp.Windows
{
    /// <summary>
    /// SettingsWindow.xaml 的交互逻辑
    /// </summary>
    public partial class SettingsWindow
    {
        public EditWatchItemDto SettingsModel { get; private set; }

        public SettingsWindowViewModel SettingsWindowViewModel { get; private set; } = new SettingsWindowViewModel();

        public SettingsWindow(EditWatchItemDto editItem = null)
        {
            SettingsWindowViewModel.Window = this;
            InitializeComponent();

            if (editItem != null)
            {
                SettingsWindowViewModel.Form = editItem;
            }

            this.DataContext = SettingsWindowViewModel;
        }
    }

    public class SettingsWindowViewModel : ObservableObject
    {
        private EditWatchItemDto _form = new EditWatchItemDto();
        public EditWatchItemDto Form
        {
            get => _form;
            set => SetProperty(ref _form, value);
        }

        public WatchItemManage WatchItemManage { get; private set; }
        public SettingsWindow Window { get; set; }

        public SettingsWindowViewModel()
        {
            WatchItemManage = App.Current.Service.GetRequiredService<WatchItemManage>();

            SelectFileCommand = new RelayCommand(() =>
            {
                var s = new OpenFileDialog();
                s.Filter = "(*.exe)|*.exe";
                s.Multiselect = false;
                if (s.ShowDialog() == DialogResult.OK)
                {
                    Form.FilePath = s.FileName;
                    Form.Name = System.IO.Path.GetFileName(s.FileName);
                    Form.AutoStart = true;
                }
            });
            SaveCommand = new RelayCommand(() =>
            {
                if (string.IsNullOrWhiteSpace(Form.FilePath))
                {
                    HandyControl.Controls.MessageBox.Show("请选择要监控的程序");
                    return;
                }

                var autoStart = Form.AutoStart;
                var a = Form.DelayStartInSecond;
                var b = Form.RestartIntervalInHour;

                if (autoStart && b > 0 && a > (b * 60 * 60))
                {
                    HandyControl.Controls.MessageBox.Show("延时启动时长超过定时启动时长");
                    return;
                }


                var item = new WatchItem()
                {
                    Id = Form.Id,
                    Remark = Form.Remark,
                    FilePath = Form.FilePath,
                    DisplayName = string.IsNullOrWhiteSpace(this.Form.FilePath) ? System.IO.Path.GetFileName(Form.FilePath) : Form.Name,
                    FileName = System.IO.Path.GetFileName(Form.FilePath),
                    AutoStart = autoStart,
                    DelayStart = (autoStart && a > 0) ? a : 0,
                    ScheduledRestart = (int)Math.Ceiling(b * 60.0)
                };

                if (Form.Id > 0)
                {
                    WatchItemManage.Update(item);
                }
                else
                {
                    WatchItemManage.Add(item);
                }
                this.Window.Close();
            });
        }

        public ICommand SaveCommand { get; }

        public ICommand SelectFileCommand { get; }
    }

    public partial class EditWatchItemDto: ObservableObject
    {
        public int Id { get; set; }

        [ObservableProperty]
        public string filePath  = string.Empty;

        [ObservableProperty]
        public string name  = string.Empty;

        [ObservableProperty]
        public bool autoStart;

        /// <summary>
        /// 延迟启动(秒)
        /// </summary>
        [ObservableProperty]
        public int delayStartInSecond;
        /// <summary>
        /// 重启间隔(小时)
        /// </summary>
        [ObservableProperty]
        public int restartIntervalInHour;

        [ObservableProperty]
        public string remark;
    }
}
