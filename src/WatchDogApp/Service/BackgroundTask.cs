﻿using CommunityToolkit.Mvvm.Messaging;
using Hardcodet.Wpf.TaskbarNotification;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using WatchDog.WatchItemManages;
using WatchDog;
using WatchDogApp.Windows;
using System.Reflection.Metadata;

namespace WatchDogApp.Service
{
    public class BackgroundTask
    {
        public TaskbarIcon TaskbarNotification { get; private set; }

        public BackgroundTask()
        {
            TaskbarNotification = App.Current.Service.GetRequiredService<TaskbarIcon>();
            TaskbarNotification.DataContext = App.Current.Service.GetRequiredService<NotifyIconViewModel>();
 
            WeakReferenceMessenger.Default.Register<ServiceStatusChangedMessage>(this, (c, m) =>
            {
                App.Current.Dispatcher.Invoke(() =>
                {
                    HandlerServiceStatusChange(m.Value);

                    SetIcon(m.Value);
                });

            });


            using (var client = new SqliteDbContext())
            {
                client.Database.EnsureCreated();
            }

            var watchItemManage = App.Current.Service.GetRequiredService<WatchItemManage>();
            watchItemManage.DataChanged += (s, e) =>
            {
                WindowsServiceManage.ExecuteCommand(ZWatchDogServiceManage.serviceName, WindowsServiceCustomCommands.WatchItemChanged);
            };

        }

        private void HandlerServiceStatusChange(int status)
        {
            if (status == 0)
            {
                var vm = GetMainWindowViewModel();
                if (vm != null)
                    vm.ServiceStatus = "守护服务未安装";
 
                TaskbarNotification.ToolTipText = "守护服务未安装";
                TaskbarNotification.IconSource = GetIcon(status);
 
            }
            else if (status == 1) //Stop
            {
                var vm = GetMainWindowViewModel();
                if (vm != null)
                    vm.ServiceStatus = "守护服务已停止";
 
                TaskbarNotification.ToolTipText = "守护服务已停止";
                TaskbarNotification.IconSource = GetIcon(status);
            }
            else if (status == 4) //Running
            {
                var vm = GetMainWindowViewModel();
                if (vm != null)
                    vm.ServiceStatus = "守护服务已启动";
                TaskbarNotification.ToolTipText = "守护服务已启动";
                TaskbarNotification.IconSource = GetIcon(status);
            }
            else if (status == 7) //Paused
            {
                var vm = GetMainWindowViewModel();
                if (vm != null)
                    vm.ServiceStatus = "守护服务已暂停";
 
                TaskbarNotification.ToolTipText = "守护服务已暂停";
                TaskbarNotification.IconSource = GetIcon(status);
            }
            else
            {
                TaskbarNotification.IconSource = GetIcon(status);
            }
        }

        public static BitmapImage GetIcon(int i)
        {
            // ServiceControllerStatus 0-Not Install,1-Stopped,2-StartPending,3-StopPending,4-Running,5-ContinuePending,6-PausePending,7-Paused
            return i switch
            {
                //case 0:
                1 => new BitmapImage(GetIconUrl(i)),
                2 => new BitmapImage(GetIconUrl(i)),
                3 => new BitmapImage(GetIconUrl(i)),
                4 => new BitmapImage(GetIconUrl(i)),
                5 => new BitmapImage(GetIconUrl(i)),
                6 => new BitmapImage(GetIconUrl(i)),
                _ => new BitmapImage(GetIconUrl(i)),
            };
        }
        public static void SetIcon(int i)
        {
            // ServiceControllerStatus 0-Not Install,1-Stopped,2-StartPending,3-StopPending,4-Running,5-ContinuePending,6-PausePending,7-Paused
            App.Current.MainWindow.Icon = i switch
            {
                1 => BitmapFrame.Create(GetIconUrl(i)),
                2 => BitmapFrame.Create(GetIconUrl(i)),
                3 => BitmapFrame.Create(GetIconUrl(i)),
                4 => BitmapFrame.Create(GetIconUrl(i)),
                5 => BitmapFrame.Create(GetIconUrl(i)),
                6 => BitmapFrame.Create(GetIconUrl(i)),
                7 => BitmapFrame.Create(GetIconUrl(i)),
                _ => BitmapFrame.Create(GetIconUrl(i)),
            };
        }
       

        public static Uri GetIconUrl(int i)
        {
            // ServiceControllerStatus 0-Not Install,1-Stopped,2-StartPending,3-StopPending,4-Running,5-ContinuePending,6-PausePending,7-Paused
            return i switch
            {
                //case 0:
                1 => new Uri("pack://application:,,,/WatchDogApp;component/Resources/logo64/logo2.ico"),
                2 => new Uri("pack://application:,,,/WatchDogApp;component/Resources/logo64/logo4.ico"),
                3 => new Uri("pack://application:,,,/WatchDogApp;component/Resources/logo64/logo4.ico"),
                4 => new Uri("pack://application:,,,/WatchDogApp;component/Resources/logo64/logo5.ico"),
                5 => new Uri("pack://application:,,,/WatchDogApp;component/Resources/logo64/logo4.ico"),
                6 => new Uri("pack://application:,,,/WatchDogApp;component/Resources/logo64/logo.ico"),
                7 => new Uri("pack://application:,,,/WatchDogApp;component/Resources/logo64/logo3.ico"),
                _ => new Uri("pack://application:,,,/WatchDogApp;component/Resources/logo64/logo.ico"),
            };
        }
 

        public static MainWindowViewModel GetMainWindowViewModel()
        {
            if (App.Current.MainWindow is MainWindow)
            {
                return App.Current.Service.GetRequiredService<MainWindowViewModel>();
            }
            return null;
        }
    }
}
