﻿using HandyControl.Controls;
using System;
using System.IO;
using System.ServiceProcess;
using WatchDog;
using WatchDog.ServiceProcess;

namespace WatchDogApp
{
    public static class ZWatchDogServiceManage
    {
        public const string serviceName = "ZWatchDog";//"ZWatchDog";
        public const string serviceFileName = "WatchDog.WinService.dll";//"WatchDog.WinService.dll";

        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// 0 - Not Install,1-Stopped,2-StartPending,3-StopPending,4-Running,5-ContinuePending,6-PausePending,7-Paused
        /// </returns>
        public static int GetStatus()
        {
            if (WindowsServiceManage.IsServiceExisted(serviceName))
            {
                var status = WindowsServiceManage.GetServiceStatus(serviceName);
                return (int)status;
            }
            else
            {
                return 0;
            }
        }


        public static void Install()
        {
            //var s = typeof(WatchDog.WinService.Service1);
            //var selvice = s.Assembly;
            string serviceFilePath = Path.Combine(Directory.GetCurrentDirectory(), serviceFileName);
            if (WindowsServiceManage.IsServiceExisted(serviceName))
            {
                var r = WindowsServiceManage.UninstallService(serviceFilePath);
                if (r != null)
                {
                    MessageBox.Error("卸载原服务时出错");
                    return;
                }
            }
 
            var r2 = WindowsServiceManage.InstallService(serviceFilePath);
            if (r2 != null)
            {
                MessageBox.Error("安装服务时出错");
                return;
            }
            else
            {
                MessageBox.Success("服务安装成功");
            }
        }
 
        public static void Uninstall()
        {
            //var s = typeof(WatchDog.WinService.Service1);
            //var selvice = s.Assembly;
            string serviceFilePath = Path.Combine(Directory.GetCurrentDirectory(), serviceFileName);
            if (WindowsServiceManage.IsServiceExisted(serviceName))
            {
                var r = WindowsServiceManage.ServiceStop(serviceName);
                if (r != null)
                {
                    MessageBox.Error("停止服务时出错");
                    return;
                }

                var r2 = WindowsServiceManage.UninstallService(serviceFilePath);
                if (r2 != null)
                {
                    MessageBox.Error("卸载服务时出错");
                    return;
                }
                else
                    MessageBox.Success("卸载服务成功");
            }
        }

        public static void Start()
        {
            
            if (WindowsServiceManage.IsServiceExisted(serviceName))
            {
                var r = WindowsServiceManage.ServiceStart(serviceName);
                if (r != null)
                {
                    MessageBox.Error( "启动服务时出错");
                    return;
                }
                else
                    MessageBox.Success("启动服务成功");
            }
        }

        public static void Stop()
        {
           
            if (WindowsServiceManage.IsServiceExisted(serviceName))
            {
                var r = WindowsServiceManage.ServiceStop(serviceName);
                if (r != null)
                {
                    MessageBox.Error( "停止服务时出错");
                    return;
                }
                else
                    MessageBox.Success("停止服务成功");
            }
        }
    }
}
