using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
namespace WatchDogApp.Controls
{

    public class SimpleText : FrameworkElement
    {
        private FormattedText _formattedText;

        static SimpleText()
        {
            SnapsToDevicePixelsProperty.OverrideMetadata(typeof(SimpleText), new FrameworkPropertyMetadata(ValueBoxes.TrueBox));
            UseLayoutRoundingProperty.OverrideMetadata(typeof(SimpleText), new FrameworkPropertyMetadata(ValueBoxes.TrueBox));

            //HorizontalContentAlignmentProperty.OverrideMetadata(typeof(UserControl), new FrameworkPropertyMetadata(HorizontalAlignment.Stretch));
            //VerticalContentAlignmentProperty.OverrideMetadata(typeof(SimpleText), new FrameworkPropertyMetadata(VerticalAlignment.Center));
        }

         

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text), typeof(string), typeof(SimpleText), new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender, OnFormattedTextInvalidated));

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register(
            nameof(TextAlignment), typeof(TextAlignment), typeof(SimpleText),
            new PropertyMetadata(default(TextAlignment), OnFormattedTextUpdated));

        public TextAlignment TextAlignment
        {
            get => (TextAlignment)GetValue(TextAlignmentProperty);
            set => SetValue(TextAlignmentProperty, value);
        }

        public static readonly DependencyProperty TextTrimmingProperty = DependencyProperty.Register(
            nameof(TextTrimming), typeof(TextTrimming), typeof(SimpleText),
            new PropertyMetadata(default(TextTrimming), OnFormattedTextInvalidated));

        public TextTrimming TextTrimming
        {
            get => (TextTrimming)GetValue(TextTrimmingProperty);
            set => SetValue(TextTrimmingProperty, value);
        }

        public static readonly DependencyProperty TextWrappingProperty = DependencyProperty.Register(
            nameof(TextWrapping), typeof(TextWrapping), typeof(SimpleText),
            new PropertyMetadata(TextWrapping.NoWrap, OnFormattedTextInvalidated));

        public TextWrapping TextWrapping
        {
            get => (TextWrapping)GetValue(TextWrappingProperty);
            set => SetValue(TextWrappingProperty, value);
        }

        public static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register(
            nameof(Foreground), typeof(Brush), typeof(SimpleText), new PropertyMetadata(Brushes.Black, OnFormattedTextUpdated));

        public Brush Foreground
        {
            get => (Brush)GetValue(ForegroundProperty);
            set => SetValue(ForegroundProperty, value);
        }

        public static readonly DependencyProperty FontFamilyProperty = TextElement.FontFamilyProperty.AddOwner(
            typeof(SimpleText),
            new FrameworkPropertyMetadata(OnFormattedTextUpdated));

        public FontFamily FontFamily
        {
            get => (FontFamily)GetValue(FontFamilyProperty);
            set => SetValue(FontFamilyProperty, value);
        }

        public static readonly DependencyProperty FontSizeProperty = TextElement.FontSizeProperty.AddOwner(
            typeof(SimpleText),
            new FrameworkPropertyMetadata(OnFormattedTextUpdated));

        [TypeConverter(typeof(FontSizeConverter))]
        public double FontSize
        {
            get => (double)GetValue(FontSizeProperty);
            set => SetValue(FontSizeProperty, value);
        }

        public static readonly DependencyProperty FontStretchProperty = TextElement.FontStretchProperty.AddOwner(
            typeof(SimpleText),
            new FrameworkPropertyMetadata(OnFormattedTextUpdated));

        public FontStretch FontStretch
        {
            get => (FontStretch)GetValue(FontStretchProperty);
            set => SetValue(FontStretchProperty, value);
        }

        public static readonly DependencyProperty FontStyleProperty = TextElement.FontStyleProperty.AddOwner(
            typeof(SimpleText),
            new FrameworkPropertyMetadata(OnFormattedTextUpdated));

        public FontStyle FontStyle
        {
            get => (FontStyle)GetValue(FontStyleProperty);
            set => SetValue(FontStyleProperty, value);
        }

        public static readonly DependencyProperty FontWeightProperty = TextElement.FontWeightProperty.AddOwner(
            typeof(SimpleText),
            new FrameworkPropertyMetadata(OnFormattedTextUpdated));

        public FontWeight FontWeight
        {
            get => (FontWeight)GetValue(FontWeightProperty);
            set => SetValue(FontWeightProperty, value);
        }

 
        //public static readonly DependencyProperty VerticalContentAlignmentProperty =
        //DependencyProperty.Register(nameof(VerticalContentAlignment), 
        //    typeof(VerticalAlignment), 
        //    typeof(SimpleText),
        //    new PropertyMetadata(VerticalAlignment.Center));

        /// <summary>
        /// VerticalContentAlignment Dependency Property.
        ///     Flags:              Can be used in style rules
        ///     Default Value:      VerticalAlignment.Top
        /// </summary>
        //[CommonDependencyProperty]
        public static readonly DependencyProperty VerticalContentAlignmentProperty =
                    DependencyProperty.Register(
                                "VerticalContentAlignment",
                                typeof(VerticalAlignment),
                                typeof(SimpleText),
                                new FrameworkPropertyMetadata(VerticalAlignment.Top),
                                new ValidateValueCallback(ValidateVerticalAlignmentValue));

        [Bindable(true), Category("Layout")]
        public VerticalAlignment VerticalContentAlignment {
            get => (VerticalAlignment)GetValue(VerticalContentAlignmentProperty);
            set => SetValue(VerticalContentAlignmentProperty, value);
        }


        internal static bool ValidateVerticalAlignmentValue(object value)
        {
            VerticalAlignment va = (VerticalAlignment)value;
            return (va == VerticalAlignment.Top
                    || va == VerticalAlignment.Center
                    || va == VerticalAlignment.Bottom
                    || va == VerticalAlignment.Stretch);
        }
 

        protected override void OnRender(DrawingContext drawingContext) => drawingContext.DrawText(_formattedText, new Point());

        //protected override void OnRender(DrawingContext drawingContext)
        //{
             

        //    Point textLocation = new Point(0, 6);
        //    drawingContext.DrawText(_formattedText,textLocation);
        //    //drawingContext.DrawRectangle(null, _adornerBorderPen,
        //    //    new Rect(CornerResizeHandleSize / 2, CornerResizeHandleSize / 2,
        //    //        _frameSize.Width - CornerResizeHandleSize, _frameSize.Height - CornerResizeHandleSize));
        //    base.OnRender(drawingContext);
        //}

        private void EnsureFormattedText()
        {
            if (_formattedText != null || Text == null)
            {
                return;
            }

            _formattedText = TextHelper.CreateFormattedText(Text, FlowDirection,
                new Typeface(FontFamily, FontStyle, FontWeight, FontStretch), FontSize,this);

            UpdateFormattedText();
        }

        private void UpdateFormattedText()
        {
            if (_formattedText == null)
            {
                return;
            }

            _formattedText.MaxLineCount = TextWrapping == TextWrapping.NoWrap ? 1 : int.MaxValue;
            _formattedText.TextAlignment = TextAlignment;
            _formattedText.Trimming = TextTrimming;

            _formattedText.SetFontSize(FontSize);
            _formattedText.SetFontStyle(FontStyle);
            _formattedText.SetFontWeight(FontWeight);
            _formattedText.SetFontFamily(FontFamily);
            _formattedText.SetFontStretch(FontStretch);
            _formattedText.SetForegroundBrush(Foreground);
        }

        private static void OnFormattedTextUpdated(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var outlinedTextBlock = (SimpleText)d;
            outlinedTextBlock.UpdateFormattedText();

            outlinedTextBlock.InvalidateMeasure();
            outlinedTextBlock.InvalidateVisual();
        }

        private static void OnFormattedTextInvalidated(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var outlinedTextBlock = (SimpleText)d;
            outlinedTextBlock._formattedText = null;

            outlinedTextBlock.InvalidateMeasure();
            outlinedTextBlock.InvalidateVisual();
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            EnsureFormattedText();

            _formattedText.MaxTextWidth = Math.Min(3579139, availableSize.Width);
            _formattedText.MaxTextHeight = Math.Max(0.0001d, availableSize.Height);

            return new Size(_formattedText.Width, _formattedText.Height);
        }

        ///// <summary>
        /////     Default control measurement is to measure only the first visual child.
        /////     This child would have been created by the inflation of the
        /////     visual tree from the control's style.
        /////
        /////     Derived controls may want to override this behavior.
        ///// </summary>
        ///// <param name="constraint">The measurement constraints.</param>
        ///// <returns>The desired size of the control.</returns>
        //protected override Size MeasureOverride(Size constraint)
        //{
        //    int count = this.VisualChildrenCount;

        //    if (count > 0)
        //    {
        //        UIElement child = (UIElement)(this.GetVisualChild(0));
        //        if (child != null)
        //        {
        //            child.Measure(constraint);
        //            return child.DesiredSize;
        //        }
        //    }

        //    return new Size(0.0, 0.0);
        //}
 
        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            int count = this.VisualChildrenCount;

            if (count > 0)
            {
                UIElement child = (UIElement)(this.GetVisualChild(0));
                if (child != null)
                {
                    child.Arrange(new Rect(arrangeBounds));
                }
            }
            return arrangeBounds;
        }
    }

    /// <summary>
    ///     装箱后的值类型（用于提高效率）
    /// </summary>
    internal static class ValueBoxes
    {
        internal static object TrueBox = true;

        internal static object FalseBox = false;

        internal static object VerticalBox = Orientation.Vertical;

        internal static object HorizontalBox = Orientation.Horizontal;

        internal static object VisibleBox = Visibility.Visible;

        internal static object CollapsedBox = Visibility.Collapsed;

        internal static object HiddenBox = Visibility.Hidden;

        internal static object Double01Box = .1;

        internal static object Double0Box = .0;

        internal static object Double1Box = 1.0;

        internal static object Double10Box = 10.0;

        internal static object Double20Box = 20.0;

        internal static object Double100Box = 100.0;

        internal static object Double200Box = 200.0;

        internal static object Double300Box = 300.0;

        internal static object DoubleNeg1Box = -1.0;

        internal static object Int0Box = 0;

        internal static object Int1Box = 1;

        internal static object Int2Box = 2;

        internal static object Int5Box = 5;

        internal static object Int99Box = 99;

        internal static object BooleanBox(bool value) => value ? TrueBox : FalseBox;

        internal static object OrientationBox(Orientation value) =>
            value == Orientation.Horizontal ? HorizontalBox : VerticalBox;

        internal static object VisibilityBox(Visibility value)
        {
            switch (value)
            {
                case Visibility.Visible: return VisibleBox;
                case Visibility.Collapsed: return CollapsedBox;


                case Visibility.Hidden: return HiddenBox;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);


            }

        }
    }

    internal class TextHelper
    {
        public static FormattedText CreateFormattedText(string text, FlowDirection flowDirection, Typeface typeface, double fontSize, Visual _this)
        {

        var formattedText = new FormattedText(
            text,
            CultureInfo.CurrentUICulture,
            flowDirection,
            typeface,
            fontSize, Brushes.Black, VisualTreeHelper.GetDpi(_this).PixelsPerDip);
 
            return formattedText;
        }
    }
}
