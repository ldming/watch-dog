﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace WatchDogApp
{
    public class ServiceStatusChangedMessage : ValueChangedMessage<int>
    {
        public ServiceStatusChangedMessage(int status) : base(status)
        {
        }
    }
}
