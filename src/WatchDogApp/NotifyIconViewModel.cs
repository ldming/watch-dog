﻿using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using Hardcodet.Wpf.TaskbarNotification;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.ComponentModel;
using System.ServiceProcess;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WatchDog;

namespace WatchDogApp
{
    public class NotifyIconViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly System.Timers.Timer _timer;
        public NotifyIconViewModel()
        {
            InstallServiceCommand = new RelayCommand(() =>
            {
                ZWatchDogServiceManage.Install();
                ServiceStatus = DateTime.UtcNow.Millisecond; //该设置值无实际意义，主要是触发状态更新
            });
            UninstallServiceCommand = new RelayCommand(() =>
            {
                ZWatchDogServiceManage.Uninstall();
                ServiceStatus = DateTime.UtcNow.Millisecond; //该设置值无实际意义，主要是触发状态更新
            });
            StartServiceCommand = new RelayCommand(() =>
            {
                ZWatchDogServiceManage.Start();
                ServiceStatus = DateTime.UtcNow.Millisecond; //该设置值无实际意义，主要是触发状态更新
            });
            StopServiceCommand = new RelayCommand(() =>
            {
                ZWatchDogServiceManage.Stop();
                ServiceStatus = DateTime.UtcNow.Millisecond; //该设置值无实际意义，主要是触发状态更新
            });
            

            _timer = new System.Timers.Timer(1000) { AutoReset = true };
            _timer.Elapsed += (sender, eventArgs) =>
            {
                ServiceStatus = DateTime.UtcNow.Millisecond; //该设置值无实际意义，主要是触发状态更新
            };
            _timer.Start();
        }

        private int _serviceStatus = -1;
        public int ServiceStatus
        {
            get
            {
                return _serviceStatus;
            }
            set
            {
                var status = ZWatchDogServiceManage.GetStatus();
                if (_serviceStatus != status)
                {
                    _serviceStatus = ZWatchDogServiceManage.GetStatus();
                    WeakReferenceMessenger.Default.Send(new ServiceStatusChangedMessage(_serviceStatus));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ServiceStatus)));
                }
            }
        }

        public ICommand ShowWindowCommand { get; } = new RelayCommand(() =>
        {
            if (App.Current.MainWindow.IsVisible == false)
            {
                App.Current.MainWindow.Show();
            }
 
            if (App.Current.MainWindow.WindowState == WindowState.Minimized)
            {
                SystemCommands.RestoreWindow(App.Current.MainWindow);
            }

            if (App.Current.MainWindow.IsActive == false)
            {
                App.Current.MainWindow.Activate();
            }


            App.Current.MainWindow.Topmost = true;
        });

        public ICommand HideWindowCommand { get; } = new RelayCommand(() => { App.Current.MainWindow.Hide(); });

        public ICommand ExitApplicationCommand { get; } = new RelayCommand(() =>
        {
            var task = App.Current.Service.GetRequiredService<TaskbarIcon>();
            task?.Dispose();
            App.Current.Shutdown();
        });

        public ICommand StopServiceCommand { get; }
        public ICommand StartServiceCommand { get; }
        public ICommand InstallServiceCommand { get; }
        public ICommand UninstallServiceCommand { get; }

 

    }
}
