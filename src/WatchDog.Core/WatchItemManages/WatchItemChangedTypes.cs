﻿namespace WatchDog.WatchItemManages
{
    public enum WatchItemChangedTypes
    {
        Add = 0,
        Update = 1,
        Del = 2,
    }
}
