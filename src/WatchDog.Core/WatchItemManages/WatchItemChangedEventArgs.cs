﻿using System;
using System.Collections.Generic;

namespace WatchDog.WatchItemManages
{
    public class WatchItemChangedEventArgs : EventArgs
    {
        /// <summary>
        /// 变更内容
        /// </summary>
        public List<WatchItem> Items { get; private set; }
        /// <summary>
        /// 0-add 1-update 2-del
        /// </summary>
        public WatchItemChangedTypes UpdateType { get; private set; }

        public WatchItemChangedEventArgs(WatchItemChangedTypes updateType, List<WatchItem> fileInfos)
        {
            Items = fileInfos;
            UpdateType = updateType;
        }
    }
}
