﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace WatchDog.WatchItemManages
{
    public class WatchItem
    {
        public int Id { get; set; }

        [MaxLength(128)]
        public string FileName { get; set; }

        [MaxLength(128)]
        public string DisplayName { get; set; }

        [MaxLength(1024)]
        public string FilePath { get; set; }
        /// <summary>
        /// 自动启动
        /// </summary>
        public bool AutoStart { get; set; }
        /// <summary>
        /// 启动时延时时长（s），有效值范围：0-定时重启范围内(如果有)
        /// </summary>
        public int DelayStart { get; set; }

        /// <summary>
        /// 定时重启（分），默认：0（不启用）
        /// </summary>
        public int ScheduledRestart { get; set; }
 
        public string Remark { get; set; }

        public DateTime LastUpdateTime { get; set; }

        [MaxLength(40)]
        [Unicode(false)]
        public virtual string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();
    }

    public class WatchItemDto
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Status { get; set; } = string.Empty;

        public string DurationInMinute { get; set; } = string.Empty;

        public string AutoStart { get; set; } = string.Empty;

        public string FilePath { get; set; } = string.Empty;

        public bool IsSelected { get; set; }


    }
}
