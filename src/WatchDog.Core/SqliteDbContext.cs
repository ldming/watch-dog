﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Linq;
using System.Reflection;
using WatchDog.WatchItemManages;

namespace WatchDog
{
    public class SqliteDbContext : DbContext
    {
        public SqliteDbContext(DbContextOptions<SqliteDbContext> options)
        : base(options)
        {
        }
        public SqliteDbContext() : base() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var assemblyFilePath = Assembly.GetExecutingAssembly().Location;
            var assemblyDirPath = Path.GetDirectoryName(assemblyFilePath);
 
            optionsBuilder.UseSqlite($"Data Source={Path.Combine(assemblyDirPath,"WatchDog.db")}");
        }

        public DbSet<WatchItem> WatchItems { get; set; }

 
        public IQueryable<WatchItem> QueryWatchItems { get { return WatchItems; } }
 
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WatchItem>(c =>
            {
                c.HasKey(a => a.Id);
                c.Property(e => e.FileName).HasMaxLength(32).IsRequired();
                c.Property(e => e.DisplayName).HasMaxLength(32).IsRequired();
                c.Property(e => e.FilePath).HasMaxLength(512).IsRequired();
                c.Property(e => e.Remark).HasMaxLength(512);
            });
        }
    }
}
