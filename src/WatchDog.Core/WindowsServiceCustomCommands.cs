﻿namespace WatchDog
{
    /// <summary>
    /// 一个应用程序定义的命令标志，指示要执行的自定义命令。该值必须介于 128 和 256 之间
    /// </summary>
    public enum WindowsServiceCustomCommands : int
    {
        /// <summary>
        /// 用于通知值守服务（WatchDog.WinService）、要守护的项目已发生变化，需要同步缓存和数据库里之间的数据。
        /// 为尽量减少值守服务对磁盘磁盘的使用，值守服务只会在服务启动时才从数据库读取配置。
        /// 启动后通过配置管理程序（WatchDog.WinForm）重新配置监控项目时，需通过发送此命令通知到值守服务。
        /// </summary>
        WatchItemChanged = 200,
        /// <summary>
        /// 通知服务端将日志记录等级变更为调试默认（记录Debug及以上）
        /// </summary>
        ServiceLogLevelDebug = 130,
        /// <summary>
        /// 通知服务端将日志记录等级变更为生产默认（记录Warning及以上）
        /// </summary>
        ServiceLogLevelWarning = 131
    }
}
