@ECHO off
cls

ECHO 删除解决方案下所有项目内的全部 BIN 和 OBJ 文件夹...
ECHO.

FOR /d /r . %%d in (bin,obj) DO (
	IF EXIST "%%d" (		 	 
		ECHO %%d | FIND /I "\node_modules\" > Nul && ( 
			ECHO.Skipping: %%d
		) || (
			ECHO.Deleting: %%d
			rd /s/q "%%d"
		)
	)
)

ECHO.
ECHO.删除完毕，按任意键退出。
pause > nul