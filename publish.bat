@echo off

cd /d %~dp0

dotnet restore src\WatchDogApp\WatchDogApp.csproj

dotnet build src\WatchDogApp\WatchDogApp.csproj -c Release

dotnet publish src\WatchDogApp\WatchDogApp.csproj -r win-x64 --self-contained true -c Release -o dist

:: set CURRENT_DATE_STRING=%date:~0,4%%date:~5,2%%date:~8,2%
:: echo CURRENT_DATE_STRING=%CURRENT_DATE_STRING%
:: set "TIME_STRING_FILL_ZREO=%time: =0%"
:: echo TIME_STRING_FILL_ZREO=%TIME_STRING_FILL_ZREO%
:: set CURRENT_TIME_STRING=%TIME_STRING_FILL_ZREO:~0,2%%TIME_STRING_FILL_ZREO:~3,2%
:: 
:: cd dist
:: 7zr.exe a -r GongWu_Api%CURRENT_DATE_STRING%%CURRENT_TIME_STRING%.7z GongWu_Api

PAUSE
