项目起源于公司有很多老服务器程序并不是Windows服务，而是基于WinForm或控制台窗口。
先前一直用的是一个第三方的看门狗程序，是基于WinForm的开发的，在服务器重启后用户未登录的情况下它自身也无法启动。
就想着改造一下，改成Windows服务，在Windows服务中启动目标EXE程序就行了。

然而，事情远没有想像的这么简单。

因为，从Windows Vista开始，服务程序位于SESSION 0，桌面程序位于SESSION 1-X，通常是1，远程桌面就是其它的数了。
就算最终实现了在Windows服务启动了带窗口的EXE程序功能，也没有UI界面。即：无法实现在用户未登录桌面的情况下实现自启，做不到开机未登录就自启。
 
所以，有相同想法的朋友们，放弃吧，这是一条死胡同，只有将改造老程序将WinForom或控制台服务器改成无界面的Windows服务，原先的界面实时显示打印功能移到日志记录或Web页面中去才是出路。

本项目分两部分
第一部分是将原来的第三方看门狗程序功能复现，将.Net Framework升级到.Net5.0，可以独立发布，不依赖于目标机器的.Net Framework环境，也不用关心版本号是多少，坏处是2003上没法用了。
第二部分就是上面提到的Windows服务版，算是踩坑记录。 

## 参考文档
https://www.cnblogs.com/bobositlife/p/10918354.html
C#/.NET基于Topshelf创建Windows服务的守护程序作为服务启动的客户端桌面程序不显示UI界面的问题分析和解决方案

https://www.cnblogs.com/CityOfThousandFires/p/10375242.html
C#穿透session隔离———Windows服务启动UI交互程序

https://www.cnblogs.com/lwqlun/p/12153935.html
在Windows服务中启动Asp.Net Core时 wwwroot目录为空 

ASP.NET Core hosted in a Windows Service - static files not being served (i.e. contents of /wwwroot)
https://stackoverflow.com/questions/60561605/asp-net-core-hosted-in-a-windows-service-static-files-not-being-served-i-e-c


